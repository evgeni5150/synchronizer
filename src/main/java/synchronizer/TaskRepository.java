package synchronizer;

import org.springframework.data.mongodb.repository.MongoRepository;

import synchronizer.entities.BpmnTask;

public interface TaskRepository extends MongoRepository<BpmnTask, String> {

}
