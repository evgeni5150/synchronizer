package synchronizer.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Reference {
	 @Id
	 private String id;
	 
	 public String executionId;
	 
	 public long issueId;

	public String getId() {
		return id;
	}
	
    public Reference() {}


	public void setId(String id) {
		this.id = id;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public long getIssueId() {
		return issueId;
	}

	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}
	 
}