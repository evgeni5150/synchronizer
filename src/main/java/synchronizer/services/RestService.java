package synchronizer.services;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Map;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.ReferenceRepository;
import synchronizer.entities.BpmnTask;
import synchronizer.entities.Reference;
import synchronizer.entities.redmine.RedmineIssue;
import synchronizer.utils.TaskUtil;

@RestController
public class RestService {

	private static final Logger logger = LoggerFactory.getLogger(RestService.class);
	

	@Value("${TELEGRAM_CHAT_ID}")
	private String TELEGRAM_CHAT_ID;

	@Value("${TELEGRAM_API_TOKEN}")
	private String TELEGRAM_API_TOKEN;

	@Value("${MAIL_RECEIVER}")
	private String MAIL_RECEIVER;

	@Autowired
	private ReferenceRepository repository;

	@Autowired
	private TaskUtil taskUtil;
	
	@Autowired
	JavaMailSender javaMailSender;

	@RequestMapping(value = "/redmine", method = RequestMethod.POST, produces = "application/json")
	public void process(@RequestBody String payload) throws Exception {

		RedmineIssue redmineIssue = taskUtil.deserializeRedmineIssue(payload);
		if (redmineIssue.getStatus().getName().equals("Resolved")) {
			long redmineIssueId = redmineIssue.getId();
			Reference ref = repository.findByIssueId(redmineIssueId);
			ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
			if (ref != null) {
				String executionId = ref.getExecutionId();
				Map<String, Object> callbackPayload = Collections.<String, Object>singletonMap("Message",
						"Issue with id" + redmineIssueId + " has been created!");

				// Send the callback to the process engine.
				processEngine.getRuntimeService().signal(executionId, callbackPayload);
				logger.info("Redmine issue with id: " + redmineIssueId + " has been completed");

				// remove reference entry from database because it's no longer needed
				repository.delete(ref);
			} else {
				logger.info("Process for redmine issue with id: " + redmineIssueId + " dont exist");

			}

		}
		// TODO else?
	}

	@Retryable(value = { RestClientException.class }, maxAttempts = 5, backoff = @Backoff(delay = 5000))
	public String sendIssue(String issueJSON, URI url) throws JsonProcessingException, InterruptedException {
		logger.info("Attempting to send the BPMN task to Issue Tracker.");
		String result = "";
		final RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.set("content-type", "application/json;charset=utf-8");

		HttpEntity<String> entity = new HttpEntity<String>(issueJSON, headers);

		result = restTemplate.postForObject(url, entity, String.class);
		return result;
	}

	@Recover
	public String recover(RestClientException t, String issueJSON) throws JsonParseException, JsonMappingException, IOException {
		// TODO Notify BPMN
		ObjectMapper mapper = new ObjectMapper();
		BpmnTask task = mapper.readValue(issueJSON, BpmnTask.class);
		logger.warn("Issue tracker is unreachable. Issue will be saved to database!");

		String text = "Issue with subject: " + task.getSubject() + " and descripton: " + task.getDescription()
				+ ", for user with id: " + task.getAssigneeId() + " with Due Date set to: " + task.getDueDate()
				+ " and launched by process with id: " + task.getProcessId()
				+ " has not been sended to Issue Tracker due to connectivity problems after 4 attempts.";

		logger.info("Issue data will be send to Telegram channel!");
		sendPendingIssueToTelegram(text);
		
		// sender and receiver identifications should be specified in application.properties
		if(!MAIL_RECEIVER.isEmpty()) {
		logger.info("Issue data will be send to the person responsible via mail!");
		sendSimpleMessage(MAIL_RECEIVER, "Issue not sended to Tracker", text);
		}
		
		// TODO send notification to Pidgin
		
		return "";
	}

	public void sendPendingIssueToTelegram(String text) {
		logger.info("Attempting to send issue to Telegram");
		String result = "";
		String apiToken = TELEGRAM_API_TOKEN;
		String chatId = TELEGRAM_CHAT_ID;
		final RestTemplate restTemplate = new RestTemplate();
		String url = "https://api.telegram.org/bot" + apiToken + "/sendMessage";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.set("content-type", "application/json;charset=utf-8");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("chat_id", chatId)
				.queryParam("text", text);
		HttpEntity<String> entity = new HttpEntity<String>(text, headers);

		result = restTemplate.postForObject(builder.build().encode().toUri(), entity, String.class);
		logger.info("Telegram bot responded with: " + result);

	}

	public void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage mail = new SimpleMailMessage();

		mail.setTo(to);
		mail.setSubject(subject);
		mail.setText(text);
		
		logger.info("Sending...");
		
		javaMailSender.send(mail);
		
		logger.info("Done!");
	}

}