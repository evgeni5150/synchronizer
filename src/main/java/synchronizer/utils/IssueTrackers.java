package synchronizer.utils;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum IssueTrackers {
    REDMINE(1),
    JIRA(2);
    
    private static final Map<Integer,IssueTrackers> lookup 
    = new HashMap<Integer,IssueTrackers>();

static {
    for(IssueTrackers s : EnumSet.allOf(IssueTrackers.class))
         lookup.put(s.getCode(), s);
}

private int code;

private IssueTrackers(int code) {
    this.code = code;
}

public int getCode() { return code; }

public static IssueTrackers get(int code) { 
    return lookup.get(code); 
}
}