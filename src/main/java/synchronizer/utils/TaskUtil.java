package synchronizer.utils;

import java.io.IOException;
import java.net.URI;

import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.ReferenceRepository;
import synchronizer.entities.redmine.RedmineBpmnTask;
import synchronizer.entities.redmine.RedmineIssue;
import synchronizer.entities.jira.JiraBpmnTask;
import synchronizer.entities.jira.JiraResponse;

@Component
public class TaskUtil {

	private static final Logger logger = LoggerFactory.getLogger(TaskUtil.class);
	// injects the Redmine's API KEY from spring boot's application.properties
	@Value("${REDMINE_API_KEY}")
	private String REDMINE_API_KEY;

	@Value("${JIRA_API_KEY}")
	private String JIRA_API_KEY;

	@Value("${REDMINE_URL}")
	private String REDMINE_URL;

	@Value("${JIRA_URL}")
	private String JIRA_URL;

	@Autowired
	ReferenceRepository repository;

	public String generateBpmnTask(ActivityExecution execution, IssueTrackers issueTracker) throws JsonProcessingException {

		// retrieve variables from the Workflow Engine
		String subject = "";
		int projectId = 1;
		String description = "";
		int assigneeId = 0;
		String dueDate = "";
		ObjectMapper mapper = new ObjectMapper();
		
		if (execution.getVariable("preDefinedSubject") != null) {
			subject = execution.getVariable("preDefinedSubject").toString();

			// delete the predefined variable after been used to avoid ambiguity
			// execution.removeVariable("preDefinedSubject");
		} else if (execution.getVariable("subject") != null) {
			subject = execution.getVariable("subject").toString();
		} else {
			 subject = execution.getCurrentActivityName();
		}

		if (execution.getVariable("preDefinedProjectId") != null) {
			projectId = Integer.parseInt(execution.getVariable("preDefinedProjectId").toString());
			// execution.removeVariable("preDefinedProjectId");
		} else if (execution.getVariable("projectId") != null) {
			projectId = Integer.parseInt(execution.getVariable("projectId").toString());
		}
		if (execution.getVariable("preDefinedDescription") != null) {
			description = execution.getVariable("preDefinedDescription").toString();
			// execution.removeVariable("preDefinedDescription");
		} else if (execution.getVariable("description") != null) {
			description = execution.getVariable("description").toString();
		}
		if (execution.getVariable("preDefinedAssigneeId") != null) {
			assigneeId = Integer.parseInt(execution.getVariable("preDefinedAssigneeId").toString());
			// execution.removeVariable("preDefinedAssigneeId");
		} else if (execution.getVariable("assigneeId") != null) {
			assigneeId = Integer.parseInt(execution.getVariable("assigneeId").toString());
		}
		if (execution.getVariable("preDefinedDueDate") != null) {
			dueDate = execution.getVariable("preDefinedDueDate").toString();
			// execution.removeVariable("preDefinedDueDate");
		} else if (execution.getVariable("dueDate") != null) {
			dueDate = execution.getVariable("dueDate").toString();
		}
		switch (issueTracker) {
		case JIRA: {
			JiraBpmnTask task = new JiraBpmnTask();
			task.setProjectId(projectId);
			task.setSubject(subject);
			task.setDescription(description);
			task.setAssigneeId(assigneeId);
			task.setProcessId(execution.getId());
			task.setDueDate(dueDate);
			return mapper.writeValueAsString(task);
		}
		case REDMINE: {
			RedmineBpmnTask task = new RedmineBpmnTask();
			task.setProjectId(projectId);
			task.setSubject(subject);
			task.setDescription(description);
			task.setAssigneeId(assigneeId);
			task.setProcessId(execution.getId());
			task.setDueDate(dueDate);
			return mapper.writeValueAsString(task);
		}
		default: return "";
		}
	}

	public RedmineIssue deserializeRedmineIssue(String issueJSON) throws JsonProcessingException {
		RedmineIssue redmineIssue = new RedmineIssue();
		ObjectMapper mapper = new ObjectMapper();
		try {
			redmineIssue = mapper.readValue(issueJSON, RedmineIssue.class);
		} catch (IOException e) {
			logger.error("Error deserializing redmine issue json.", e);
		}
		return redmineIssue;
	}

	public JiraResponse deserializeJiraIssue(String issueJSON) throws JsonProcessingException {
		JiraResponse jiraResponse = new JiraResponse();
		ObjectMapper mapper = new ObjectMapper();
		try {
			jiraResponse = mapper.readValue(issueJSON, JiraResponse.class);
		} catch (IOException e) {
			logger.error("Error deserializing jira issue json.", e);
		}
		return jiraResponse;
	}

	public URI generateUrl(IssueTrackers issueTracker) {
		String url = "";
		String apiKey = "";

		switch (issueTracker) {
		case JIRA:
			url = JIRA_URL.endsWith("/") ? JIRA_URL.substring(url.length() - 1) : JIRA_URL;
			url = url + "/rest/api/2/issue/";
			apiKey = JIRA_API_KEY;
		case REDMINE:
			url = REDMINE_URL.endsWith("/") ? REDMINE_URL.substring(url.length() - 1) : REDMINE_URL;
			url = url + "/issues.json";
			apiKey = REDMINE_API_KEY;
		}
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("key", apiKey);

		return builder.build().encode().toUri();
	}
}
