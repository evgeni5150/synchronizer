package synchronizer.jobs;

import java.util.Collections;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;

import synchronizer.ReferenceRepository;
import synchronizer.entities.BpmnTask;
import synchronizer.entities.redmine.RedmineIssue;
import synchronizer.entities.Reference;
import synchronizer.services.RestService;
import synchronizer.utils.TaskUtil;

public class SendIssuesJob implements Job {

	static Logger logger = LogManager.getLogger(SendIssuesJob.class);
	
	// TODO Autowired objects should be injected in the bean manually
	
	@Autowired
	private ReferenceRepository repository;

	@Autowired
	private TaskUtil taskUtil;

	@Autowired
	private RestService restService;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		// job disabled for the moment until we decide should we use quartz at all
		
		/*logger.info("Quartz job sending issue to the tracker started!");
		Reference ref = new Reference();
		JobDataMap data = context.getJobDetail().getJobDataMap();
		String task = (String) data.get("issue");
		try {
			
			String result = restService.sendIssue(task);
			RedmineIssue redmineIssue = taskUtil.deserializeRedmineIssue(result);
			ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
			String executionId = task.getProcessId();

			// store issue and process id as reference to database so the process can be
			// signaled later
			ref.setExecutionId(executionId);
			ref.setRedmineIssueId(redmineIssue.getId());
			repository.save(ref);

			Map<String, Object> callbackPayload = Collections.<String, Object>singletonMap("Message", "Some message");

			// Send the callback to the process engine.
			processEngine.getRuntimeService().signal(executionId, callbackPayload);

		} catch (JsonProcessingException | InterruptedException | RestClientException e) {
			logger.error("Connection attempt by Quartz Scheduler failed. ", e);
		}*/
		// TODO what happens when all connection attempts fail?
	}

}