package synchronizer;

import org.springframework.data.mongodb.repository.MongoRepository;

import synchronizer.entities.Reference;

public interface ReferenceRepository extends MongoRepository<Reference, String> {

    public Reference findByIssueId(long issueId);

}
