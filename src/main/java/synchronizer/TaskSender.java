package synchronizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import org.camunda.bpm.engine.impl.bpmn.behavior.AbstractBpmnActivityBehavior;
import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonProcessingException;

import synchronizer.entities.Reference;
import synchronizer.services.RestService;
import synchronizer.utils.IssueTrackers;
import synchronizer.utils.TaskUtil;

@Component
public class TaskSender extends AbstractBpmnActivityBehavior {

	private static final Logger logger = LoggerFactory.getLogger(TaskSender.class);
	@Autowired
	private ReferenceRepository repository;

	@Autowired
	private TaskUtil taskUtil;

	@Autowired
	private RestService restService;

	public void execute(final ActivityExecution execution) throws Exception {
		
		// TODO leave the execution as earliest as possible
		String executionId = execution.getId();
		long issueId = 0;
		Reference ref = new Reference();
		int issueTrackerId = 1;
		String result = "";
		if (execution.getVariable("preDefinedIssueTracker") != null) {
			issueTrackerId = Integer.parseInt(execution.getVariable("preDefinedIssueTracker").toString());
		} else if (execution.getVariable("issueTracker") != null) {
			issueTrackerId = Integer.parseInt(execution.getVariable("issueTracker").toString());
		}
		try {
			IssueTrackers issueTracker = IssueTrackers.get(issueTrackerId);
			URI url = taskUtil.generateUrl(issueTracker);
			String taskJSON = taskUtil.generateBpmnTask(execution, issueTracker);
			result = restService.sendIssue(taskJSON, url);
			if (result.isEmpty()) {
				return;
			}
			logger.info("The External Service recieved results: " + result);
			switch (issueTracker) {
			case JIRA:
				issueId = taskUtil.deserializeJiraIssue(result).getId();
			case REDMINE:
				issueId = taskUtil.deserializeRedmineIssue(result).getId();
			}

			execution.setVariable("issueId", issueId);
			logger.info("Issue with id: " + issueId + " has been created!");

			// store issue and process id as reference to database so the process can be
			// signaled later
			ref.setExecutionId(executionId);
			ref.setIssueId(issueId);
			repository.save(ref);
		}
		// disable Qaurtz Job Scheduler
		/*
		 * catch (RestClientException e) {
		 * 
		 * logger.info("Http request fail. Issue tracker is unreachable.");
		 * 
		 * // quartz job that sends issues to the tracker JobDetail job =
		 * JobBuilder.newJob(SendIssuesJob.class) .withIdentity("sendIssuesJob" +
		 * executionId, "redmineJobs").build(); job.getJobDataMap().put("issue", task);
		 * // quartz trigger firing 3 times within 8 hours Trigger trigger =
		 * TriggerBuilder.newTrigger() .withIdentity("issueTrigger" + executionId,
		 * "redmineTriggers") //
		 * .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(4).
		 * withRepeatCount(2))
		 * .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(3)
		 * .withRepeatCount(2)) // launch faster just for test .build();
		 * 
		 * // schedule the job Scheduler scheduler = new
		 * StdSchedulerFactory().getScheduler(); scheduler.start();
		 * scheduler.scheduleJob(job, trigger); logger.info("Quartz job with key " +
		 * job.getKey().getName() + " has been scheduled."); }
		 */

		catch (InterruptedException e) {
			logger.error("Thread interrupted exception ", e);

		} catch (JsonProcessingException e) {
			logger.error("Error with serializing issue java object to json. ", e);

		}

	}

	public void signal(ActivityExecution execution, String signalName, Object signalData) throws Exception {

		// leave the service task activity:
		leave(execution);
	}

}