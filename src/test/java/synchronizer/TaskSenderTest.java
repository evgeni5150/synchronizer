package synchronizer;

import static org.junit.Assert.assertNotNull;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import synchronizer.services.RestService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskSenderTest {

	private static final String TEST_PROCESS_KEY = "Process_1";

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	TaskService taskService;

	private MockMvc mockMvc;

	@Autowired
	private RestService restServiceMock;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.standaloneSetup(restServiceMock).build();
	}
	
	@Test
	public void testTaskSender() throws Exception {
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(TEST_PROCESS_KEY);
		assertNotNull(processInstance);
		Task task1 = taskService.createTaskQuery().taskDefinitionKey("Test_Task1")
				.processInstanceId(processInstance.getId()).singleResult();
		assertNotNull(task1);

		taskService.complete(task1.getId());
		
		// Service Task 2 is running
		
		assertNotNull(runtimeService.getVariable(processInstance.getId(), "issueId"));
		long redmineId = (long) runtimeService.getVariable(processInstance.getId(), "issueId");

		String issue = "{\"issue\":{\"id\":" + redmineId
				+ ",\"project\":{\"id\":1,\"name\":\"test\"},\"tracker\":{\"id\":1,\"name\":\"Bug\"},\"status\":{\"id\":1,\"name\":\"Resolved\"},\"priority\":{\"id\":2,\"name\":\"Normal\"},\"author\":{\"id\":1,\"name\":\"Evgeni Admin\"},\"subject\":\"nov test\",\"start_date\":\"2018-08-28\",\"done_ratio\":0,\"created_on\":\"2018-08-28T10:58:25Z\",\"updated_on\":\"2018-08-28T10:58:25Z\"}}";

		mockMvc.perform(post("/redmine").contentType(MediaType.APPLICATION_JSON).content(issue))
				.andExpect(status().isOk());

		Task task3 = taskService.createTaskQuery().taskDefinitionKey("Test_Task3")
				.processInstanceId(processInstance.getId()).singleResult();
		assertNotNull(task3);

		runtimeService.deleteProcessInstance(processInstance.getId(), "JUnit test");
	}

}