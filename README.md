# Redmine Integration for BPMN (Camunda)

Custom asynchronous service tasks that synchronizes Camunda BPMN Engine with Redmine issue tracker. 

## Getting Started

### Prerequisites


 * Redmine
 * MongoDB
 * Camunda Modeler
 * Docker


### Installing

First you need to setup your issue tracker identifiers. Open application.properties (its located in the resources folder) find  ISSUE\_TRACKER\_URL, delete the default address and type in the address of the issue tracker that you want to connect Camunda with. In the same file you should set your issue tracker's profile API_KEY. In Redmine the user's API key is located in My Account. Make sure you use the API key of a user with admin rights.

The project uses Camunda Spring Boot Starter that gives you preconfigured local deployment of Camunda on the Spring Boot's embedded Tomcat container.  Run the executable file. Then open localhost:8080 and you will find your local instance of Camunda deployed and ready to go. 

### Running with Docker

You can also run the application directly with Docker.

Build with maven: ``mvn install dockerfile:build``

Run it: `` docker run -p 8080:8080 --net="host" -t isoft/synchronizer ``

### Features

This project makes it easy to:

 * Create custom tasks that are synchronised with Redmine
 * Make sure you don't loose any data in the process

It comes with: 

 * Custom service task template for Camunda Modeler. 
 * Java based web service
  
    
### How it works?

The custom service task template can be used by the Camunda modeler. Create a simple Service Task and the Element Template dropdown will appear in the Service Task's Properties Panel. Custom template's name is "Synchronized". Choose "Synchronized" from the dropdown and a couple of custom fields will appear in the panel. This is the place to define the issue specific fields like subject and description. You can leave some of them blank or all of them blank, and generate them as process variables during the execution. 

![Process diagram](src/main/resources/process.png?raw=true)

Deploy your process and start instance of it. When the process execution reaches a task that is defined as "Synchronized" it calls a Java Delegate class called RedmineSender. It decouples the business logic from the running process instance context and calls a simple Web Service that takes care of the communication with the issue tracker's REST API and keeping the data in its own database during that time so you can't loose any data if the issue tracker is unreachable. When the issue is created in the issue tracker, it is configured to send a callback on status change using webhook. 

When the issue status is changed, WebHook sends post request to the Web Service. The Web Service checks the status of the issue and if it's "resolved" it signals the process that it should leave the service task activity. 

If the Issue Tracker is unreachable at the moment of request, the data is stored to the database and a Quartz Scheduler Jobs is scheduled. The job is responsible to try to send the issue to the tracker and is executed three times in the interval of 4 hours.  

## Built With

* [Spring Boot](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/) - The java framework used
* [Camunda Spirng Boot Starter](https://docs.camunda.org/manual/develop/user-guide/spring-boot-integration/) - Camunda BPM auto configuration for standalone process application
* [Maven](https://maven.apache.org/) - Dependency Management
* [MongoDB](https://docs.mongodb.com/) - NoSQL database to store references, jobs and triggers 
* [Quartz](http://www.quartz-scheduler.org/documentation/) - Used as retry mechanism for post requests to the issue tracker